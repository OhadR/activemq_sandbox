package com.ohadr.activemq.receiver;
 
import java.util.Enumeration;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.TextMessage;
 
import org.apache.activemq.ActiveMQConnectionFactory;

import com.ohadr.activemq.Commons;
 
public class MessageReceiver {
 
    // default broker URL is : tcp://localhost:61616"
 
 
    public static void main(String[] args) throws JMSException {
        ActiveMQConnectionFactory connectionFactory =
        		//new ActiveMQConnectionFactory("vm://localhost");
        		new ActiveMQConnectionFactory(Commons.url);
        readMessage(connectionFactory);
    }

	static String readMessage(ConnectionFactory connectionFactory) throws JMSException {
		// Getting JMS connection from the server
        Connection connection = connectionFactory.createConnection();
        connection.start();
 
        // Creating session for seding messages
        Session session = connection.createSession(false,
                Session.AUTO_ACKNOWLEDGE);
 
        // Getting the queue 'JCG_QUEUE'
        Destination destination = session.createQueue(Commons.subject);
 
        // MessageConsumer is used for receiving (consuming) messages
        MessageConsumer consumer = session.createConsumer(destination);
 
        // Here we receive the message.
        Message message = consumer.receive();
 
        String retVal = null;
        // We will be using TestMessage in our example. MessageProducer sent us a TextMessage
        // so we must cast to it to get access to its .getText() method.
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            retVal = textMessage.getText();
            System.out.println("Received message '" + textMessage.getText() + "'");
        }
        connection.close();
        return retVal;
	}
    
    /**
     * peek - do not remove the message from the Queue.
     * @throws Exception
     */
    public static String peek() throws Exception {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Commons.url);
    	return peek(connectionFactory);
    }

    /**
     * peek - do not remove the message from the Queue.
     * @throws Exception
     */
    public static String peek(ConnectionFactory connectionFactory) throws Exception {

        Connection connection = connectionFactory.createConnection();
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Queue destination = session.createQueue( Commons.subject );		//destination is 'ActiveMQDestination'
        QueueBrowser browser = session.createBrowser(destination);
        Enumeration enumeration = browser.getEnumeration();

        String retVal = null;
        
        if (enumeration.hasMoreElements()) {
            Object msg = enumeration.nextElement();
            TextMessage m = (TextMessage) msg;
            System.out.println(" !!!!!!!!Browsed  msg " +m.getText());
            retVal = m.getText();
        }

        browser.close();
        session.close();
        connection.close();
        return retVal;

    }

}