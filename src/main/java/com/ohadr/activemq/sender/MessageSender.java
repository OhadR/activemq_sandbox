package com.ohadr.activemq.sender;
 
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
 
import org.apache.activemq.ActiveMQConnectionFactory;

import com.ohadr.activemq.Commons;
 
public class MessageSender {
     
    // default broker URL is : tcp://localhost:61616"
     
    public static final String MESSAGE_CONTENT = "Hello !!! Welcome to the world of ActiveMQ.";

	public static void main(String[] args) throws JMSException {   
    	sendMessage();
    }
    
    static void sendMessage() throws JMSException
    {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Commons.url);
    	sendMessage(connectionFactory);
    }

    static void sendMessage(ConnectionFactory connectionFactory) throws JMSException
    {
        // Getting JMS connection from the server and starting it
        Connection connection = connectionFactory.createConnection();
        connection.start();
         
        //Creating a non transactional session to send/receive JMS message.
        Session session = connection.createSession(false,
                Session.AUTO_ACKNOWLEDGE);  
         
        //Destination represents here our queue 'JCG_QUEUE' on the JMS server. 
        //The queue will be created automatically on the server.
        Destination destination = session.createQueue(Commons.subject); 
         
        // MessageProducer is used for sending messages to the queue.
        MessageProducer producer = session.createProducer(destination);
         
        // We will send a small text message saying 'Hello World!!!' 
        TextMessage message = session
                .createTextMessage(MESSAGE_CONTENT);
         
        // Here we are sending our message!
        producer.send(message);
         
        System.out.println(Commons.subject + " @@ '" + message.getText() + "'");
        producer.close();
        session.close();
        connection.close();
    }
}