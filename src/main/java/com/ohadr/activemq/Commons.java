package com.ohadr.activemq;

import org.apache.activemq.ActiveMQConnection;

public abstract class Commons {

    // Name of the queue we will send/receive messages from
    public static String subject = "JCG_QUEUE";

    //URL of the JMS server. DEFAULT_BROKER_URL will just mean that JMS server is on localhost
    public static String url = 
//    		ActiveMQConnection.DEFAULT_BROKER_URL;
//    		"vm://localhost:61616?broker.persistent=false&broker.deleteAllMessagesOnStartup=true"; 
//    		"vm://localhost:61616?broker.persistent=true&broker.deleteAllMessagesOnStartup=true"; 
//    		"vm://localhost:61616"; //all messages are stored across tests. 
//    		"vm://localhost:61616?broker.deleteAllMessagesOnStartup=true"; 
//    		"vm://localhost"; //identical to "vm://localhost:61616". all messages are stored across tests. 
//    		"vm://XXXlocalhost";  	//identical to 'localhost'	
    		"vm://localhost?broker.deleteAllMessagesOnStartup=true"; 

}
