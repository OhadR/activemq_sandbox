package com.ohadr.activemq.sender;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.Test;

import com.ohadr.activemq.Commons;
import com.ohadr.activemq.receiver.MessageReceiver;

public class SendMessageTest  
{
    private ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Commons.url);

    @Test
    public void testA() throws Exception {

        // Getting JMS connection and keep it open till the end of test, so broker will not die:
        Connection connection = connectionFactory.createConnection();

        MessageSender.sendMessage(connectionFactory);
        
        //verify message was sent
    	String message = MessageReceiver.peek(connectionFactory);
        assertNotNull("response cannot be null", message);
        assertTrue(message.equals(MessageSender.MESSAGE_CONTENT));
        
        connection.close();
    }

    @Test
    public void testB() throws Exception {
    	
        //verify queue is empty
    	String message = MessageReceiver.peek();
        assertNull("response expected to be null", message);
    }
}
