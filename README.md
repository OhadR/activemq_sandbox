# activeMQ sandbox

<img src="Apache-activemq-logo.png" alt="ActiveMQ-Logo" height="70px" />


## A word about `EmbeddedActiveMQBroker`

In unit tests, you may want each test to run with its own embedded broker. For this case, we have [`EmbeddedActiveMQBroker`](http://activemq.apache.org/how-to-unit-test-jms-code.html). According to its documentations, it is enough to add the `@Rule` and the embedded broker would be in use; However from what I understand this is not the case. I saw that I must force the embedded ConnectionFactory to be in use by using it explicitly.

So if I use "regular" `ActiveMQConnectionFactory` in the tests, I see that the messages are written to the real ActiveMQ. But if I use `EmbeddedActiveMQBroker.createConnectionFactory()`, then messages are not written to the real ActiveMQ, so I can assume that the embedded Queue is in use. (see @SendMessageTest, commit [e8b714b])

## embedded-broker

Eventually I understood that **there is no real need in `EmbeddedActiveMQBroker`**. (This is why I have removed it on commit[1b26e40]). When the brokerUrl is "vm", and the Broker Service is down (i.e. activemq is not running), then ActiveMQ runs "embedded broker". This broker can be configured not to persist messages, or to clear all messages on startup:

    url = "vm://localhost?broker.persistent=false&broker.deleteAllMessagesOnStartup=true";

See [VM Transport](http://activemq.apache.org/vm-transport-reference).

Note that The broker will be created upon creation of the first connection, and killed upon closure of the last connection. This is why in my test `SendMessageTest.testA()` the broker was created before the `send()`, and killed right after (because there were no other open connections). Then, when I called `peek()` *on the same unit-test* and the deleteAllOnStartup was set to true, it found no data. But if the deleteAllOnStartup was false (default) the message was found. This is because peek() caused another creation of (another embedded) broker.

To avoid that, I have created a connection in the beginning of the test and kept is open till the end of the test. This forced the broker not to be killed, so I could use the 'deleteAllOnStartup=true' without failing the test. the 'persistence=false' helped me clean the broker across tests.

Technically, when embedded Q is in use, a folder 'activemq-data' is created, and underneath it there is a folder with the name of the Q - e.g. 'localhost' (could be 'moshe' as well). There KahaDB stores its files and data.

## References

http://vvratha.blogspot.com/2012/05/java-client-to-sendreceive-messages-for.html

https://examples.javacodegeeks.com/enterprise-java/jms/jms-messagelistener-example/

EmbeddedActiveMQBroker: http://activemq.apache.org/how-to-unit-test-jms-code.html

[VM Transport](http://activemq.apache.org/vm-transport-reference)


# JaCoCo (jacoco-maven-plugin)

JaCoCo is a great tool to enfore coverage, but we would like to make sure it collects not only the unit-tests, but the integration tests as well. 

The initial declaration of the jacoco-plugin is:

```xml
<plugin>
    <groupId>org.jacoco</groupId>
    <artifactId>jacoco-maven-plugin</artifactId>
    <version>0.8.2</version>
    <executions>
        <execution>
            <id>jacoco-initialize</id>
            <goals>
                <goal>prepare-agent</goal>
            </goals>
        </execution>
        <execution>
            <id>jacoco-site</id>
            <phase>package</phase>
            <goals>
                <goal>report</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

in this case, it does not collect the test-coverage of the integration tests (because the i-tests are in a later phase, called 'integration-test', unline the unit tests that are in the 'test' phase):

![Capture jacoco before](Capture jacoco before.JPG)


so we change the declaration, according to the post below:

```xml
<plugin>
    <groupId>org.jacoco</groupId>
    <artifactId>jacoco-maven-plugin</artifactId>
    <version>0.8.2</version>
    <executions>
         ... as above, plus:
        
        <!--
            Prepares the property pointing to the JaCoCo runtime agent which
            is passed as VM argument when Maven the Failsafe plugin is executed.
        -->
        <execution>
            <id>pre-integration-test</id>
            <phase>pre-integration-test</phase>
            <goals>
                <goal>prepare-agent</goal>
            </goals>
        </execution>
        <!--
            Ensures that the code coverage report for integration tests after
            integration tests have been run.
        -->
        <execution>
            <id>post-integration-test</id>
            <phase>post-integration-test</phase>
            <goals>
                <goal>report</goal>
            </goals>
        </execution>
        
    </executions>
</plugin>
```

Now, the converage from the *ITest is collected as well. The ITest tests `com.ohadr.activemq.receiver.non_blocking.ConsumerMessageListener`, and we can see this in the report:

![Capture jacoco after](Capture jacoco after.JPG)




[Creating Code Coverage Reports for Unit and Integration Tests With the JaCoCo Maven Plugin](https://www.petrikainulainen.net/programming/maven/creating-code-coverage-reports-for-unit-and-integration-tests-with-the-jacoco-maven-plugin/)